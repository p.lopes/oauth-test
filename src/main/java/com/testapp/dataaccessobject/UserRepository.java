package com.testapp.dataaccessobject;


import com.testapp.domainobject.UserDO;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<UserDO, Long> {

    UserDO findByUsername(String username);

}
