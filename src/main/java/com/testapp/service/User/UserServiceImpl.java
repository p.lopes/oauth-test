package com.testapp.service.User;

import com.testapp.dataaccessobject.UserRepository;
import com.testapp.domainobject.UserDO;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;

@Service
public class UserServiceImpl implements UserService{

    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) { this.userRepository = userRepository; }

    private UserDO findUserChecked(Long userId)
    {
        return userRepository.findById(userId)
                .orElseThrow(() -> new EntityNotFoundException("Could not find entity with id: " + userId));
    }

    @Override
    public UserDO find(Long userId) {
        return findUserChecked(userId);
    }

    @Override
    public UserDO create(UserDO userDO) {
        PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String password = ("{bcrypt}" + passwordEncoder.encode(userDO.getPassword()));
        userDO.setPassword(password);
        userRepository.save(userDO);
        return userDO;
    }
}
