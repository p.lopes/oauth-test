package com.testapp.service.User;

import com.testapp.domainobject.UserDO;

public interface UserService {
    UserDO find(Long userId);

    UserDO create(UserDO userDO);
}
