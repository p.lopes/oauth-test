package com.testapp.controller;

import com.testapp.domainobject.UserDO;
import com.testapp.service.User.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("v1/users")
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(final UserService userService) { this.userService = userService; }

    @GetMapping("/{userId}")
    public UserDO getUser(@Valid @PathVariable Long userId)
    {
        return userService.find(userId);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public UserDO createUser(@RequestBody UserDO userDO){
        return userService.create(userDO);
    }
}
